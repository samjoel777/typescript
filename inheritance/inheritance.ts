class WhiteBoard {
    public color : string;
    public size : number;
    public writeOnBoard = (): void =>{
        console.log("WhiteBoard");
    };
}

class Pen extends WhiteBoard {
    public content : string;
    public company : string;
    public writeViaPen = (): void =>{
        console.log("Pen");
        console.log(`
        Details:
        Color : ${this.color}
        Size : ${this.size}
        Content : ${this.content}
        Company : ${this.company}
        `);
    }
}

class Ink extends Pen{
    public ink : string;
    public inkComapny : string;
    public fillInk = () =>{
        console.log("Ink");
        console.log(`
        Details:
        Color : ${this.color}
        Size : ${this.size}
        Content : ${this.content}
        Company : ${this.company}
        Ink : ${this.ink}
        InkCompany : ${this.inkComapny}
        `);
    }
}

let ink = new Ink();

ink.color = "blue";
ink.size = 7;
ink.content = "Best Ink";
ink.company = "camel";
ink.ink = "No";
ink.inkComapny = "Faber Castle";

ink.fillInk();
