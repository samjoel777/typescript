//ECMASCRIPT 5+  

class Bank {
    public bankName : string;
    private bankBranch : string
    private bankIFSC : string
    private bankBalance : number;

    public get IFSC (): string {
        return this.bankIFSC;
    }

    public set IFSC (ifsc) {
        if(ifsc.length ===9){
        this.bankIFSC = ifsc;
        }else{
            this.bankIFSC = "invalid IFSC";
        }
    }

    public get Branch (): string {
        return this.bankBranch;
    }

    public set Branch (branch) {
        this.bankBranch = branch;
    }

    public get Balance (): number{
        return this.bankBalance;
    }

    public set Balance (balance){
        if(balance >=0){
            this.bankBalance = balance;
        }else{
            throw new Error ("Negative is not allowed")
        }
    }
}

let bank = new Bank();

bank.bankName = "SAM BANK";

// Normal ------> Accessing

// bank.setIFSC("SAMJOEL77"); 
// bank.setBranch("KKD"); 
// bank.setBalance(100000);

// ECMASCRIPT 5+  ---->  Accessing

bank.IFSC = "SAMJOEL77"; 
bank.Branch = "KKD"; 
bank.Balance = 100000

console.log(`
Bank Details:
Bank Name: ${bank.bankName}
Bank Branch: ${bank.Branch}
Bank IFSC: ${bank.IFSC}
Bank Balance: ${bank.Balance}
`);

// Normal 

// Bank Branch: ${bank.getBranch()}
// Bank IFSC: ${bank.getIFSC()}
// Bank Balance: ${bank.getBalance()}