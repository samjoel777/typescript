export class NewCompany {
    public static cname = "JOHN&JOHN";
    private assets : string;
    public address : string;
    constructor(assets:string,address:string){
        this.assets = assets
        this.address = address;
    }

    // constructor(public name:string,private assets:string,public address:string){}

    public showInfo () : void {
        console.log(`
        Welcome to ${NewCompany.cname} at ${this.address}
        `)
    }
    public evaluateAssets ():void {
        console.log(`
        The Company assets are ${this.assets}
        `);
    }

    // static method only access static properties
    public static showName(): void{
        console.log(`
        The Company name is ${NewCompany.cname}`);
    }
}

// Empty Functions
export class Dept{}
export class Employee{}
export function sampleFun(): void {
    console.log("Iam Sample Function");
}  


//We can access any object outside the class -----> static methods(variable & function)
NewCompany.cname = "FIRE&ICE"; // overwriting

let companyA = new NewCompany("$77777","USA");
companyA.showInfo();
companyA.evaluateAssets();

let companyB = new NewCompany("$99999","UK");
companyB.showInfo();
companyB.evaluateAssets();