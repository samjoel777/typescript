"use strict";
exports.__esModule = true;
var NewCompany = /** @class */ (function () {
    function NewCompany(assets, address) {
        this.assets = assets;
        this.address = address;
    }
    // constructor(public name:string,private assets:string,public address:string){}
    NewCompany.prototype.showInfo = function () {
        console.log("\n        Welcome to " + NewCompany.cname + " at " + this.address + "\n        ");
    };
    NewCompany.prototype.evaluateAssets = function () {
        console.log("\n        The Company assets are " + this.assets + "\n        ");
    };
    // static method only access static properties
    NewCompany.showName = function () {
        console.log("\n        The Company name is " + NewCompany.cname);
    };
    NewCompany.cname = "JOHN&JOHN";
    return NewCompany;
}());
exports.NewCompany = NewCompany;
// Empty Functions
var Dept = /** @class */ (function () {
    function Dept() {
    }
    return Dept;
}());
exports.Dept = Dept;
var Employee = /** @class */ (function () {
    function Employee() {
    }
    return Employee;
}());
exports.Employee = Employee;
function sampleFun() {
    console.log("Iam Sample Function");
}
exports.sampleFun = sampleFun;
//We can access any object outside the class -----> static methods(variable & function)
NewCompany.cname = "FIRE&ICE"; // overwriting
var companyA = new NewCompany("$77777", "USA");
companyA.showInfo();
companyA.evaluateAssets();
var companyB = new NewCompany("$99999", "UK");
companyB.showInfo();
companyB.evaluateAssets();
