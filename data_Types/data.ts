// Number
// let sample1:number;
// sample1 = 777;
let sample1 : number = 777;

// string
// let sample2 : string;
// sample2 = "SAM";
let sample2 : string = "SAM";

// boolean
// let sample3 : boolean;
// sample3 = true;
let sample3 : boolean = true;

// null
// let sample4 : null;
// sample4 = null;
let sample4 : null = null;


// undefined
// let sample5 : undefined;
// sample5 = undefined;
let sample5 : undefined = undefined;

// any
// let sample6 : ny;
// sample6 = string/number/boolean etc;
let sample6 : any = 123;

// array
let sample7 : number[] = [123,456,789]
let sample8 : string[] = ["A","B","C"]

// tuple   ---> Fixed Array Objects
let sample9 : [string] = ["SAM"]
let sample10 : [string,number] = ["SAM",123]
let sample11 : [string,string,boolean] = ["SAM","JOEL",true]

// Enum ---> group of constants
enum person {
    John = "Cricketer",
    Mike = "Actor",
    Will = "Physician",
    Smith = "Musician",
    ip = "127.0.0.1"
} 

enum person1 {
    John,Mike,Will,Smith,ip 
} 

enum person2{
    John,Mike=100,Will,Smith,ip 
}

console.log(person.John); // Already value assigned
console.log(person1.John); // No value assigned ---> auto incremental value
console.log(person2.Will); // indexing value for Mike --->incremental from where it is assigned

