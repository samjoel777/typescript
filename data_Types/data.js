// Number
// let sample1:number;
// sample1 = 777;
var sample1 = 777;
// string
// let sample2 : string;
// sample2 = "SAM";
var sample2 = "SAM";
// boolean
// let sample3 : boolean;
// sample3 = true;
var sample3 = true;
// null
// let sample4 : null;
// sample4 = null;
var sample4 = null;
// undefined
// let sample5 : undefined;
// sample5 = undefined;
var sample5 = undefined;
// any
// let sample6 : ny;
// sample6 = string/number/boolean etc;
var sample6 = 123;
// array
var sample7 = [123, 456, 789];
var sample8 = ["A", "B", "C"];
// tuple   ---> Fixed Array Objects
var sample9 = ["SAM"];
var sample10 = ["SAM", 123];
var sample11 = ["SAM", "JOEL", true];
// Enum ---> group of constants
var person;
(function (person) {
    person["John"] = "Cricketer";
    person["Mike"] = "Actor";
    person["Will"] = "Physician";
    person["Smith"] = "Musician";
    person["ip"] = "127.0.0.1";
})(person || (person = {}));
var person1;
(function (person1) {
    person1[person1["John"] = 0] = "John";
    person1[person1["Mike"] = 1] = "Mike";
    person1[person1["Will"] = 2] = "Will";
    person1[person1["Smith"] = 3] = "Smith";
    person1[person1["ip"] = 4] = "ip";
})(person1 || (person1 = {}));
var person2;
(function (person2) {
    person2[person2["John"] = 0] = "John";
    person2[person2["Mike"] = 100] = "Mike";
    person2[person2["Will"] = 101] = "Will";
    person2[person2["Smith"] = 102] = "Smith";
    person2[person2["ip"] = 103] = "ip";
})(person2 || (person2 = {}));
console.log(person.John); // Already value assigned
console.log(person1.John); //No value assigned ---> auto incremental value
console.log(person2.Will);
