class Company {
    public static cname = "JOHN&JOHN";
    private assets : string;
    public address : string;
    constructor(assets:string,address:string){
        this.assets = assets
        this.address = address;
    }

    // constructor(public name:string,private assets:string,public address:string){}

    public showInfo () : void {
        console.log(`
        Welcome to ${Company.cname} at ${this.address}
        `)
    }
    public evaluateAssets ():void {
        console.log(`
        The Company assets are ${this.assets}
        `);
    }

    // static method only access static properties
    public static showName(): void{
        console.log(`
        The Company name is ${Company.cname}`);
    }
}

//We can access any object outside the class -----> static methods(variable & function)
Company.cname = "FIRE&ICE"; // overwriting

let company1 = new Company("$77777","USA");
company1.showInfo();
company1.evaluateAssets();

let company2 = new Company("$99999","UK");
company2.showInfo();
company2.evaluateAssets();