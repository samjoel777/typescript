abstract class SemiStrictParent {
    business : string;
    cars : string;
    land : string;
    abstract study(): string;
    abstract school :  string;
    runBusiness (isOk) :string{
        if(isOk){
            return "GO TAKEOVER !";
        }else{
            return "DO SOMETHING ON YOUR OWN";
        }
    };
}

class Kid extends SemiStrictParent{
    school : string;

    study ():string {
        return "YES I WILL STUDY";
    }

    //polymorphism ----overloading not allowed -----overwriting is allowed
    
    runBusiness(isOk:boolean): string{
        return "YES I'M OK WITH IT";
    }
}

let k = new Kid();

var res1 = k.study();
var res2 = k.runBusiness(true);

console.log(res1);
console.log(res2);
