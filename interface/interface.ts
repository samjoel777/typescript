interface Requirement {
    name : string;
    details : string;
    duration : string;
}

interface Client {
    name : string;
    requirement : Requirement;
    login() : string;            // empty body functions // only declaration
    registration() : string;
}

class Provider implements Client {
    name :string;
    requirement : Requirement;
    login = (): string => {
        return "Login Successful";
    }
    registration = (): string => {
        return "Registration Successful"
    }
}

let p = new Provider();
var res = p.login();
console.log(res);