let sample : any = "Hello TS"; // string value given but we cannot implement string methods ex: sample.length

var tmp = <string>sample; // Type Assertion (Injecting Data Type)

console.log(tmp.length);

console.log(tmp.charAt(3));

