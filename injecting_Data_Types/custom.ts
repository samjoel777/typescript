interface CarParts {
    name : string,
    count : number,
    cost : number
}

var makeCar = (parts:CarParts) : any =>
{
    if(parts.name && parts.count &&parts.cost)
    {
        return {
            car_Name : "Koinsegg",
            car_Cost : 2000
        }
    }
    else{
        return "No Parts No Car!"
    }
}

var res1 = makeCar({
    name: "Engine",
    count : 1,
    cost : 200
});

var res2 = makeCar({
    name: "Gear",
    count : 1,
    cost: 100
});

console.log(res1);
