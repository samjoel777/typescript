interface Power {
    name : string,
    type : string,
    level : number,
    duration : number,
    isReal : boolean
}

class Hero {
    name : string;
    power : Power;


    helpingPeople = (isSerious:boolean): Power =>{
        if(this.name && isSerious)
        {
            return this.power;
        }
    }

}

let hero:Hero = new Hero();
hero.name = "Superman";
hero.power = {
    name:"Flying",
    type: "natural",
    level: 5,
    duration: 200,
    isReal : true
}

var res = hero.helpingPeople(true);
console.log(`
The Hero is :: ${hero.name},
and Power is :: ${hero.power.name},
help people with :: ${JSON.stringify(res)}
`);