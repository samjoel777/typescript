interface Address {
    street : string;
    pincode : number;
    city : string;
}

interface BAccount {
    account_Name : string;
    account_Id : number;
    account_Email : string;
    account_Balance : number;
    account_IFSC : string;
    account_Branch : string; 
}

class Bank {
    bankName : string;
    bankAdd : Address;
    bankAccounts : [BAccount]

    showBalance = (account_Id:number,acc) =>{
        let account = this.findAccount(account_Id,acc)
        return account.account_Balance;
    }

    findAccount = (account_Id:number,acc): BAccount =>{
        for(acc of this.bankAccounts){
            if(acc.account_Id === account_Id)
            {
                return acc;
            }
        }
    } 
}

let bank = new Bank();
bank.bankName = "SAM BANK";
bank.bankAccounts = [{
    account_Name : "SAM",
    account_Id : 311177110136,
    account_Email : "sam@gmail.com",
    account_Balance : 100000,
    account_IFSC : "HYD777",
    account_Branch : "Narsingi" 
}];

var res = bank.showBalance(311177110136,"acc");

console.log(res);