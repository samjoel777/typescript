let users = [
    {
        account_Name : "SAM JOEL",
        account_Id : 311177110136,
        account_Email : "sam@gmail.com",
        account_Balance : 10000,
        account_IFSC : "HYD143"
    },
    {
        account_Name : "ABDUL JANI",
        account_Id : 311177110140,
        account_Email : "jani@gmail.com",
        account_Balance : 9000,
        account_IFSC : "HYD420"
    },
    {
        account_Name : "IRFAN SHARIFF",
        account_Id : 311177110028,
        account_Email : "irfan@gmail.com",
        account_Balance : 8000,
        account_IFSC : "HYD786"
    }
]

interface Users{
    account_Name : string,
    account_Id : number,
    account_Email : string,
    account_Balance : number,
    account_IFSC : string
}

let accountName_To_AccountNumber = (account_Name,i) => 
{
    for(i=0 ; i < users.length ; i++ )
    {
        if(users[i].account_Name == account_Name)
        {
            return users[i].account_Id;
        }
    }
}

let accountNo_To_AccountName = (account_No,i) => 
{
    for(i=0 ; i < users.length ; i++ )
    {
        if(users[i].account_Id == account_No)
        {
            return users[i].account_Name;
        }
    }
}

let same_Account_Details = (account_Name,account_No) =>
{
    if(accountName_To_AccountNumber(account_Name,0) == account_No && accountNo_To_AccountName(account_No,0) == account_Name)
    {
        return true;
    }
    
}

let accountNo_To_Index = (account_No,i) => 
{
    for(i=0 ; i < users.length ; i++ )
    {
        if(users[i].account_Id == account_No)
        {
            return i;
        }
    }
}

class Bank{
    name : string;
    user : Users;

    showBalance = (name:string,i) : any => 
    {
        for(i=0 ; i < users.length ; i++ )
        {
            if(users[i].account_Name == name)
            {
                return users[i].account_Balance;
            }
        }
    }

    withdraw = (account_Name,account_No,) : any =>{
        if(same_Account_Details(account_Name,account_No) == true)
        {
            let amount;

            let index = accountNo_To_Index(account_No,0);

            amount = 1000

            users[index].account_Balance = users[index].account_Balance + amount;

            console.log(users[index]);
            return (`Amount Deposited : ${amount}`);
        }

        else
        {
            return "Given Account Details Are Incorrect";
        }
    }


}

let bank:Bank = new Bank();
// var res1 = bank.showBalance("IRFAN SHARIFF",0);
// console.log(res1);

var res2 = bank.withdraw("SAM JOEL",311177110136);
console.log(res2);
