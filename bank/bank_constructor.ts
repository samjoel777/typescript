interface Adress {
    street : string;
    pincode : number;
    city : string;
}

interface BAccount {
    account_Name : string;
    account_Id : number;
    account_Email : string;
    account_Balance : number;
    account_IFSC : string;
    account_Branch : string; 
}

class Bank {
    bankName : string;
    bankAdd : Adress;
    bankAccount : [BAccount]

    constructor(bname?:string,baddress?:Adress,baccount?: [BAccount]){
        this.bankName = bname;
        this.bankAdd = baddress;
        this.bankAccount = baccount;
    }

    getBankName = ():string =>{
        return this.bankName
    }

    showBalance = (account_Id:number) =>{
        let account = this.findAccount(account_Id)
        return account.account_Balance;
    }

    findAccount = (account_Id:number): BAccount =>{
        for(let account of this.bankAccount){
            if(account.account_Id === account_Id)
            {
                return account;
            }
        }
    } 
}

let bank = new Bank("SAM BANK",{
    street : "Ameerpet",
    pincode : 5333003,
    city : "HYD"
},[{
    account_Name : "JOEL",
    account_Id : 311177110136,
    account_Email : "sam@gmail.com",
    account_Balance : 100000,
    account_IFSC : "HYD777",
    account_Branch : "Narsingi" 
}]);

var res = bank.findAccount(311177110136);

console.log(res);